import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobileapp/agendar_exame.dart';
import 'agendar_exame.dart';
import 'package:mobileapp/data_personal.dart';
import 'data_personal.dart';
import 'package:mobileapp/main.dart';
import 'main.dart';

// ignore: must_be_immutable
class LoggedUserTest extends StatelessWidget {
  String email;
  LoggedUserTest(this.email, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Área logada'),
        actions: <Widget>[
          PopupMenuButton(
            onSelected: (result) {
              if (result == 0) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const AgendarExame()),
                );
              }
              if (result == 1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DataPersonal(email)),
                );
              }
              if (result == 2) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyApp()),
                );
              }
            },
            icon: const Icon(Icons.menu),
            itemBuilder: (context) => [
              const PopupMenuItem(
                child: Text('Agendar exame'),
                value: 0,
              ),
              const PopupMenuItem(
                child: Text("Meus dados"),
                value: 1,
              ),
              const PopupMenuItem(
                child: Text("Sair"),
                value: 2,
              ),
            ],
          ),
        ],
      ),
      body: Container(
          alignment: Alignment.topCenter,
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              const Text(
                'Meus exames agendados:',
                style: TextStyle(
                  fontSize: 22,
                  height: 2,
                  fontWeight: FontWeight
                      .bold, //line height 200%, 1= 100%, were 0.9 = 90% of actual line height
                  color: Colors.black87, //font color
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(10),
                  child: MyCustomForm(email)),
            ],
          )),
    );
  }
}

// ignore: must_be_immutable
class MyCustomForm extends StatefulWidget {
  String email;
  MyCustomForm(this.email, {Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    // ignore: no_logic_in_create_state
    return MyCustomFormState(email);
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  String email;
  // ignore: prefer_typing_uninitialized_variables
  var resp;
  MyCustomFormState(this.email);

  @override
  void initState() {
    super.initState();
    Uri url = Uri.http("localhost:3001", "/users/find/email");

    http.post(url, body: {
      "email": email,
    }).then((value) {
      // ignore: avoid_print
      print("resposta do back:" + value.body);

      Map<String, dynamic> map = jsonDecode(value.body);
      resp = map.toString();
///////////////////////////////////////////////////////////////////////////////
      Uri url2 = Uri.http("localhost:3001", "/agendamentos/${map['_id']}");
      http.get(url2).then((value) {
        // ignore: avoid_print
        print("resposta do back URL2:" + value.body);

        Map<String, dynamic> map2 = jsonDecode(value.body);

        // ignore: avoid_print
        print("EXAMESSS AGENDADOSS: " + map2.toString());
      });
/////////////////////////////////////////////////////////////////////////////////////////////////
    });
  }

  @override
  Widget build(BuildContext context) {
    return const Text('teste');
  }
}
