import 'package:flutter/material.dart';
import 'package:mobileapp/agendar_exame.dart';
import 'agendar_exame.dart';
import 'package:mobileapp/data_personal.dart';
import 'data_personal.dart';

// ignore: must_be_immutable
class LoggedUser extends StatelessWidget {
  String email;
  LoggedUser(this.email, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Área logada'),
        actions: <Widget>[
          PopupMenuButton(
            onSelected: (result) {
              if (result == 0) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const AgendarExame()),
                );
              }
              if (result == 1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DataPersonal(email)),
                );
              }
            },
            icon: const Icon(Icons.menu),
            itemBuilder: (context) => [
              const PopupMenuItem(
                child: Text('Agendar exame'),
                value: 0,
              ),
              const PopupMenuItem(
                child: Text("Meus dados"),
                value: 1,
              ),
              const PopupMenuItem(
                child: Text("Sair"),
                value: 2,
              ),
            ],
          ),
        ],
      ),
      body: Container(
        alignment: Alignment.topCenter,
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            const Text(
              'Meus exames agendados:',
              style: TextStyle(
                fontSize: 22,
                height: 2,
                fontWeight: FontWeight
                    .bold, //line height 200%, 1= 100%, were 0.9 = 90% of actual line height
                color: Colors.black87, //font color
              ),
            ),
            SizedBox(
                height: 250, //height of button
                width: 350,
                child: Card(
                  child: Column(
                    children: const [
                      Text(
                        'Exame de urina',
                        style: TextStyle(
                            fontSize: 18,
                            height: 2,
                            fontWeight: FontWeight
                                .bold //line height 200%, 1= 100%, were 0.9 = 90% of actual line height
                            ),
                      ),
                    ],
                  ),
                )),
            SizedBox(
                height: 250, //height of button
                width: 350,
                child: Card(
                  child: Column(
                    children: const [
                      Text(
                        'Exame de sangue',
                        style: TextStyle(
                            fontSize: 18,
                            height: 2,
                            fontWeight: FontWeight
                                .bold //line height 200%, 1= 100%, were 0.9 = 90% of actual line height
                            ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
