import 'dart:convert';
import 'dart:html';

import 'package:flutter/material.dart';
import 'package:mobileapp/logged_user_teste.dart';
import 'package:http/http.dart' as http;
import 'logged_user_teste.dart';

main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(home: LabExamesApp());
  }
}

class LabExamesApp extends StatelessWidget {
  const LabExamesApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Exam Lab',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Exam Lab'),
        ),
        body: Container(
          alignment: Alignment.topCenter,
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.all(10),
                child: Text('Exam Lab - Seu laboratório de Exames'),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Image.asset(
                  'assets/images/logo.png',
                  width: 150,
                  height: 150,
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(10),
                child: Text('Faça seu login'),
              ),
              const Padding(padding: EdgeInsets.all(10), child: MyCustomForm()),
            ],
          ),
        ),
      ),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController senhaController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: emailController,
            decoration: const InputDecoration(
              icon: Icon(Icons.email),
              hintText: 'Coloque seu email',
              labelText: 'Email',
            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Por favor insira um email válido';
              }
              return null;
            },
          ),
          TextFormField(
            controller: senhaController,
            obscureText: true,
            decoration: const InputDecoration(
              icon: Icon(Icons.password),
              hintText: 'Coloque sua senha',
              labelText: 'Senha',
            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Por favor insira sua senha';
              }
              return null;
            },
          ),
          Container(
            padding: const EdgeInsets.only(top: 30.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.lightBlue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                      30), //to set border radius to button
                ),
              ),
              child: const Text('Login'),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  // ignore: avoid_print
                  print(emailController.text);
                  // ignore: avoid_print
                  print(senhaController.text);

                  Uri url = Uri.http("localhost:3001", "/sign-in");

                  http.post(url, body: {
                    "email": emailController.text,
                    "password": senhaController.text
                  }).then((value) {
                    // ignore: avoid_print
                    print(value.body);

                    Map<String, dynamic> map = jsonDecode(value.body);

                    if (map['userData'] == 'user') {
                      String email = emailController.text;
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => LoggedUserTest(email)));
                    } else {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text(
                                  'Não foi possível realizar o login'),
                              content: const Text('Email ou senha incorretos!'),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context); //close Dialog
                                  },
                                  child: const Text('Fechar'),
                                )
                              ],
                            );
                          });
                    }
                  });
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
