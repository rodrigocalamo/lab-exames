import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

// ignore: must_be_immutable
class DataPersonal extends StatelessWidget {
  String email;
  DataPersonal(this.email, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Meus dados'),
      ),
      body: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Padding(
                  padding: const EdgeInsets.all(10),
                  child: MyCustomForm(email)),
            ],
          )),
    );
  }
}

// ignore: must_be_immutable
class MyCustomForm extends StatefulWidget {
  String email;
  MyCustomForm(this.email, {Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    // ignore: no_logic_in_create_state
    return MyCustomFormState(email);
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  String email;

  MyCustomFormState(this.email);
  final _formKey = GlobalKey<FormState>();
  TextEditingController primeiroNomeController = TextEditingController();
  TextEditingController ultimoNomeController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController senhaController = TextEditingController();

  @override
  void initState() {
    super.initState();

    Uri url = Uri.http("localhost:3001", "/users/find/email");

    http.post(url, body: {
      "email": email,
    }).then((value) {
      // ignore: avoid_print
      print("resposta do back:" + value.body);

      Map<String, dynamic> map = jsonDecode(value.body);

      for (int i = 0; i <= map.length; i++) {
        // ignore: avoid_print
        print("FIRST NAME: " + map['firstName']);
        // ignore: avoid_print
        print("LAST NAME: " + map['lastName']);
        // ignore: avoid_print
        print("PASSWORD NAME: " + map['password']);
        primeiroNomeController.text = map['firstName'];
        ultimoNomeController.text = map['lastName'];
        senhaController.text = map['password'];
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: primeiroNomeController,
            decoration: const InputDecoration(
              hintText: 'Primeiro nome',
              labelText: 'Primeiro nome',
            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Por favor insira seu primeiro nome';
              }
              return null;
            },
          ),
          TextFormField(
            controller: ultimoNomeController,
            decoration: const InputDecoration(
              hintText: 'Último nome',
              labelText: 'Último nome',
            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Por favor insira seu último nome';
              }
              return null;
            },
          ),
          TextFormField(
            controller: senhaController,
            //obscureText: true,
            decoration: const InputDecoration(
              hintText: 'Senha',
              labelText: 'Senha',
            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Por favor insira sua senha';
              }
              return null;
            },
          ),
          Container(
            padding: const EdgeInsets.only(top: 30.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.lightBlue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                      30), //to set border radius to button
                ),
              ),
              child: const Text('Salvar'),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  // ignore: avoid_print
                  print(primeiroNomeController.text);
                  // ignore: avoid_print
                  print(ultimoNomeController.text);

                  // ignore: avoid_print
                  print(senhaController.text);

                  Uri url = Uri.http("localhost:3001", "/users/email");

                  http.post(url, body: {
                    "email": email,
                    "firstName": primeiroNomeController.text,
                    "lastName": ultimoNomeController.text,
                    "password": senhaController.text
                  }).then((value) {
                    // ignore: avoid_print
                    print(value.body);

                    Map<String, dynamic> resp = jsonDecode(value.body);

                    if (resp['contact'] == 'updated') {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text('Atualização de dados'),
                              content: const Text(
                                  'Suas informações foram atualizadas com sucesso'),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context); //close Dialog
                                  },
                                  child: const Text('Ok'),
                                )
                              ],
                            );
                          });
                    } else {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text('Atualização de dados'),
                              content: const Text(
                                  'Não foi possível atualizar suas informações'),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context); //close Dialog
                                  },
                                  child: const Text('Fechar'),
                                )
                              ],
                            );
                          });
                    }
                  });
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
