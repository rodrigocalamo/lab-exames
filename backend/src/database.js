const mongoose = require('mongoose')

const DB_USER = process.env.DB_USER
const DB_PASS = process.env.DB_PASS
const DB_NAME = process.env.DB_NAME

const uri = 
    `mongodb+srv://${DB_USER}:${DB_PASS}@cluster0.gkdl9.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`

async function connect() {
    try {
        await mongoose.connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    })
    console.log('Database: Connected')
    } catch (err) {
        console.error(err)
    }
}

module.exports =  { connect }