const { Router } = require('express')
const router = Router()

const Exames = require('../models/Exames')

// retrieve all exams
router.get('/exams', async (req, res) => {
    try {
        const exams = await Exames.find()
        res.json(exams)
    } catch (err) {
        console.log(err)
        res.sendStatus(500).json({ error: err.message})
    }
})

// retrieve a specific exam
router.get('/exams/:id', async (req, res) => {
    const examId = req.params.id
    console.log(examId)

    Exames.findById(examId, (err, docs) => {
        if (err) {
            console.log(err)
            res.sendStatus(500).json({ error: err.message })
        } else {
            res.json(docs)
        }
    })    
})

// create a new exam
router.post('/exams', async (req, res) => {
    try {
        console.log(req.body)
        const { name, amount: { currency, value }, description } = req.body

        await Exames.create({
            name: name,
            amount: {
                currency,
                value
            },
            description: description
        })

        res.json({ resp: 'created' })
    } catch (err) {
        console.log(err)
        res.sendStatus(422).json({ error: err.message })
    }
})

// update an existing exam
router.post('/update/exams/:id', async (req, res) => {

    const exam_id = req.params.id
    const updateExam = { ...req.body }
    delete updateExam._id

    if (updateExam.description === '') {
        delete updateExam.description
    }

    if (updateExam.amount.value === '') {
        delete updateExam.amount.value
    }
    
    if (updateExam.name === '') {
        delete updateExam.name
    }

    Exames.findByIdAndUpdate(exam_id, updateExam, (err, docs) => {
        if (err) {
            console.log(err)
            res.sendStatus(422).json({ error: err.message })        
        } else {
            console.log(docs)
            res.json({ resp: 'updated' })        
        }
    })
})

// delete an exam
router.get('/delete/exams/:id', async (req, res) => {
    console.log(req.params)
    const examId = req.params.id

    Exames.findByIdAndDelete(examId, (err, docs) => {
        if (err) {
            console.log(err)
            res.sendStatus(500).json({ error: err.message })
        } else {
            res.json({ resp: 'deleted' })
        }
    })
})

module.exports = router