const { Router } = require('express')
const router = Router()
const ejs = require('ejs')
const User = require('../models/Usuario')
const sgMail = require('@sendgrid/mail')

router.post('/sign-up', async (req, res) => {
    try {
        console.log(req.body)
        let resp = ''
        const { firstName, lastName, email, password } = req.body
    
        const checkEmail = await User.findOne({
            email: email
        })

        if (checkEmail === null) {
            const createUser = await User.create({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password
            })

            const sendEmail = await ejs.renderFile('../templates/email-confirmation.ejs', { createUser })

            const msg = {
                to: email,
                from: {
                    email: process.env.EXAM_LAB_EMAIL,
                    name: process.env.EXAM_LAB_NAME
                },
                subject: `Confirme seu e-mail ${firstName}`,
                html: sendEmail
            }

            await sgMail.send(msg)
            
            resp = 'created'
        } else {
            resp = 'already registered'
        }
        res.json({ resp: resp })
    } catch (err) {
        console.log(err)
        res.sendStatus(422).json({ error: err.message })
    }
})

module.exports = router