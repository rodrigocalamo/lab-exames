const { Router } = require('express')
const router = Router()
const ejs = require('ejs')
const Usuario = require('../models/Usuario')
const sgMail = require('@sendgrid/mail')

router.get('/email-confirmation/:id', async (req, res) => {
    const userId = req.params.id
    
    Usuario.findByIdAndUpdate(userId, { isLocked: false }, async (err, docs) => {
        if (err) {
            console.log(err)
            res.sendStatus(500).json({ error: err.message }) 
        } else {
            const welcomeEmail = await ejs.renderFile('../templates/welcome.ejs', {
                docs,
                link: 'https://google.com.br'
            })

            const msg = {
                to: docs.email,
                from: {
                    email: process.env.EXAM_LAB_EMAIL,
                    name: process.env.EXAM_LAB_NAME
                },
                subject: `Bem-vindo ${docs.firstName}`,
                html: welcomeEmail
            }

            await sgMail.send(msg)

            res.json({ user: 'unlocked', email: 'sent' })
        }
    })
})

module.exports = router