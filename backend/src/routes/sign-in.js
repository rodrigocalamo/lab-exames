const { Router } = require('express')
const router = Router()
const ejs = require('ejs')
const Usuario = require('../models/Usuario')
const sgMail = require('@sendgrid/mail')

router.post('/sign-in', async (req, res) => {
    try {

        console.log(req.body)
        const { email, password } = req.body

        let userData = ''

        const findUser = await Usuario.findOne({
            email: email,
            password: password
        })

        if (findUser !== null) {
            if (findUser.isLocked == true) {
                userData = 'inactive'
            } else if (findUser.administrator == true) {
                userData = 'administrator'
            } else {
                userData = 'user'
            }
        } else {
            userData = ''
        }

        res.json({ userData: userData })
    } catch (err) {
        console.log(err)
        res.status(500).json({ error: err })
    }
})

router.post('/re-send-email', async (req, res) => {
    try {
        console.log(req.body)
        let email = ''

        const userEmail = req.body.email

        const findUser = await Usuario.findOne({
            email: userEmail
        })

        console.log(findUser)
        if (findUser !== null) {
            const sendEmail = await ejs.renderFile('../templates/second-confirmation.ejs', { findUser })
            
            const msg = {
                to: findUser.email,
                from: {
                    email: process.env.EXAM_LAB_EMAIL,
                    name: process.env.EXAM_LAB_NAME
                },
                subject: `Confirme seu e-mail ${findUser.firstName}`,
                html: sendEmail
            }

            await sgMail.send(msg)
            email = 'sent'
        } else {
            console.log('no users found.')
            email = 'not found'
        }

        res.json({ email: email })
    } catch (err) {
        console.log(err)
        res.status(500).json({ error: err })
    }
})

module.exports = router