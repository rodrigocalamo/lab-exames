const { Router } = require('express')
const router = Router()
const ejs = require('ejs')
const Usuario = require('../models/Usuario')
const sgMail = require('@sendgrid/mail')

router.post('/request-reset', async (req, res) => {
    console.log(req.body)
    try {
        const email = req.body.email

        const findUser = await Usuario.findOne({
            email: email
        })

        let resp = '' 
        console.log(findUser)
        if (findUser !== null) {
            const sendEmail = await ejs.renderFile('../templates/reset-password.ejs', { findUser })
                
                const msg = {
                    to: findUser.email,
                    from: {
                        email: process.env.EXAM_LAB_EMAIL,
                        name: process.env.EXAM_LAB_NAME
                    },
                    subject: `Redefina sua Senha`,
                    html: sendEmail
                }

            await sgMail.send(msg)
            
            resp = 'sent'
        } else {
            console.log('User not found')
        }

        res.json({ resp: resp })
    } catch (err) {
        console.log(err)
        res.status(500).json({ error: err })
    }
})

router.post('/reset-password', async (req, res) => {
    console.log(req.body)

    const { id, password } = req.body

    const updateUser = {
        _id: id,
        password: password
    }

    Usuario.findByIdAndUpdate(id, updateUser, (err, docs) => {
        if (err) {
            console.log(err)
            res.sendStatus(422).json({ error: err.message })        
        } else {
            console.log(docs)
            res.json({ updated: docs })
        }
    })
})

module.exports = router