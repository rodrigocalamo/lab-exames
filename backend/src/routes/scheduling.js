const { Router } = require('express')
const router = Router()

const ejs = require('ejs')
const sgMail = require('@sendgrid/mail')

const Exames = require('../models/Exames')
const Apontamentos = require('../models/Apontamentos')
const Agendamentos = require('../models/Agendamentos')
const Usuario = require('../models/Usuario')

// Create Exam Schedule and Send confirmation Email
router.post('/create/agendamento', async (req, res) => {
    try {
        const { examId, userId, schedule: { day, time } } = req.body

        let resp = ''
    
        const findSchedule = await Agendamentos.findOne({
            user: userId,
            exam: examId,
            schedule: {
                day: day,
                time: time
            }
        })
    
        if (findSchedule === null) {
            const findApontamento = await Apontamentos.findOne({
                exam: examId,
                schedule: {
                    day: day,
                    time: time
                }
            })

            if (findApontamento !== null) {
                const createSchedule = await Agendamentos.create({
                    user: userId,
                    exam: examId,
                    schedule: {
                        day: day,
                        time: time
                    }
                })
                
                await Apontamentos.findByIdAndDelete(findApontamento._id)
                const user = await Usuario.findById(userId)
                const exam = await Exames.findById(examId)
        
                const sendEmail = await ejs.renderFile('../templates/agendamento-confirmacao.ejs', {
                    createSchedule,
                    user,
                    exam
                })
        
                const msg = {
                    to: user.email,
                    from: {
                        email: process.env.EXAM_LAB_EMAIL,
                        name: process.env.EXAM_LAB_NAME
                    },
                    subject: `Confirmação de Agendamento do seu Exame`,
                    html: sendEmail
                }
        
                await sgMail.send(msg)
        
                resp = 'Agendamento criado com sucesso'
            } else {
                resp = 'Não há horários disponíveis neste dia'
            }
        } else {
            resp = 'Este Usuário já tem o mesmo agendamento marcado no mesmo horário'
        }

        res.json({ resp: resp })
    } catch (err) {
        console.log(err)
        res.status(500).json({ error: err.message })
    }
})

// Cancel Schedule and create appointment again
router.get('/cancelar-agendamento/:id', async (req, res) => {
    const id = req.params.id

    Agendamentos.findByIdAndDelete(id, async (err, docs) => {
        if (err) {
            console.log(err)
            res.status(500).json({ error: err.message })
        } else {
            const createApontamento = await Apontamentos.create({
                exam: docs.exam,
                schedule: {
                    day: docs.schedule.day,
                    time: docs.schedule.time
                }
            })

            const user = await Usuario.findById(docs.user)

            const sendEmail = await ejs.renderFile('../templates/cancelar-agendamento.ejs', { user })
    
            const msg = {
                to: user.email,
                from: {
                    email: process.env.EXAM_LAB_EMAIL,
                    name: process.env.EXAM_LAB_NAME
                },
                subject: `Agendamento Cancelado`,
                html: sendEmail
            }
    
            await sgMail.send(msg)

            res.json({ deleted: docs, created_apontamento: createApontamento, email: 'sent' })
        }
    })
})

// Retrieve all schedules from database
router.get('/agendamentos', async (req, res) => {
    try {
        const agendamentos = await Agendamentos.find()

        let response = []

        for (const agendamento of agendamentos) {
            const user = await Usuario.findById(agendamento.user)

            const exam = await Exames.findById(agendamento.exam)

            const schedule = {
                schedule: agendamento,
                firstName: user.firstName,
                lastName: user.lastName,
                examName: exam.name
            }

            response.push(schedule)
        }
        
        console.log(response)

        res.json(response)
    } catch (err) {
        console.log(err)
        res.status(500).json({ error: err.message })
    }
})

router.get('/agendamentos/:id', async (req, res) => {
    try {
        const userId = req.params.id

        const findSchedule = await Agendamentos.find({
            user: userId
        })

        let response = []

        if (findSchedule) {
            for (const fSchedule of findSchedule) {
                const user = await Usuario.findById(fSchedule.user)

                const exam = await Exames.findById(fSchedule.exam)

                const schedule = {
                    schedule: fSchedule,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    examName: exam.name
                }

                response.push(schedule)
            }
        }
        console.log(response)

        res.json(response)
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
})

module.exports = router