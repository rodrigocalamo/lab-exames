const { Router } = require('express')
const router = Router()

const Apontamentos = require('../models/Apontamentos')

// Create a date to a specific exam
router.post('/apontamentos/:id', async (req, res) => {
    try {
        const examId = req.params.id

        const create = await Apontamentos.create({
            exam: examId,
            schedule: {
                day: req.body.schedule.day,
                time: req.body.schedule.time
            }
        })

        res.json({ created: create })
    } catch (err) {
        console.log(err)
        res.sendStatus(422).json({ error: err.message })
    }
})

// Retrieve all appointments of hour from db
router.get('/apontamentos', async (req, res) => {
    try {
        const apontamentos = await Apontamentos.find()
        res.json({ Apontamentos: apontamentos })
    } catch (err) {
        console.log(err)
        res.sendStatus(500).json({ error: err.message })
    }
})

// Retrieve all appointments of hour from db
router.get('/apontamentos/:id', async (req, res) => {
    try {
        const examId = req.params.id

        const horarios = await Apontamentos.find({
            exam: examId
        })

        res.json(horarios)
    } catch (err) {
        console.log(err)
        res.sendStatus(500).json({ error: err.message })
    }
})

// Delete Apontamentos by Id
router.delete('/apontamentos/:id', async (req, res) => {
    const id = req.params.id

    Apontamentos.findByIdAndDelete(id, (err, docs) => {
        if (err) {
            console.log(err)
            res.sendStatus(500).json({ error: err.message })
        } else {
            res.json({ deleted: docs })
        }
    })
})


module.exports = router