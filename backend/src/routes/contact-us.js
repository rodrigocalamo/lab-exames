const { Router } = require("express");
const router = Router();
const ejs = require("ejs");
const sgMail = require("@sendgrid/mail");

router.post("/contact-us", async (req, res) => {
  try {
    console.log(req.body)

    const { email, name, subject } = req.body

    const sendEmail = await ejs.renderFile("../templates/contact-us.ejs", {
      body: req.body
    })
  
    const msg = {
      to: process.env.EXAM_LAB_EMAIL,
      from: {
        email: process.env.EXAM_LAB_EMAIL,
        name: name,
      },
      subject: subject,
      html: sendEmail,
    }

    await sgMail.send(msg)

    const clientEmail = await ejs.renderFile("../templates/clientEmail.ejs", { body: req.body })
  
    const clientMessage = {
      to: email,
      from: {
        email: process.env.EXAM_LAB_EMAIL,
        name: process.env.EXAM_LAB_NAME,
      },
      subject: `${name}, recebemos sua mensagem`,
      html: clientEmail,
    }

    await sgMail.send(clientMessage)

    res.json({ emails: 'sent'})
  } catch (err) {
    console.log(err)
    res.status(500).json({ error: err })
  }
})

module.exports = router;
