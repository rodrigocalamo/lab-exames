const { Router } = require('express')
const router = Router()

const Usuario = require('../models/Usuario')

router.get('/users/all', async (req, res) => {
    try {
        const users = await Usuario.find()
        res.json(users)
    } catch (err) {
        console.log(err)
        res.status(500).json({ error: err.message })
    }
})

// find an user by Id
router.get('/users/:id', async (req, res) => {
    try {
        const userId = req.params.id
        
        const user = await Usuario.findOne({
            _id: userId
        })

        if (user === null) {
            res.json({ error: 'usuário não encontrado'})
        } else {
            res.json({ user: user})
        }
    } catch (err) {
        console.log(err)
        res.status(422).json({ error: err.message })
    }
})

// update an existing user
router.patch('/users/:id', async (req, res) => {

    const user_id = req.params.id
    const updateUser = { ...req.body}
    delete updateUser._id

    Usuario.findByIdAndUpdate(user_id, updateUser, (err, docs) => {
        if (err) {
            console.log(err)
            res.status(422).json({ error: err.message })        
        } else {
            console.log(docs)
            res.json({ updated: docs })        
        }
    })
})

//find user by email
router.post('/users/find/email', async (req, res) => {
    console.log(req.body)

    Usuario.findOne({ email: req.body.email }, (err, docs) => {
        if (err) {
            console.log(err)
            res.status(422).json(err.message)
        } else {
            res.json(docs)
        }
    })
})

// update an existing user by email
router.post('/users/email', async (req, res) => {
    console.log(req.body)

    const update = {
        isLocked: req.body.isLocked,
        administrator: req.body.administrator
    }

    if (req.body.firstName !== '' || !req.body.firstName) update.firstName = req.body.firstName

    if (req.body.lastName !== '' || !req.body.lastName) update.lastName = req.body.lastName

    if (req.body.password !== ''|| !req.body.password) update.password = req.body.password

    Usuario.findOneAndUpdate({ email: req.body.email }, update, (err, docs) => {
        if (err) {
            console.log(err)
            res.status(422).json({ error: err.message })  
        } else {
            res.json({ contact: 'updated' })   
        }
    })
})

// delete an user
router.delete('/users/:id', async (req, res) => {
    const user_id = req.params.id

    Usuario.findByIdAndDelete(user_id, (err, docs) => {
        if (err) {
            console.log(err)
            res.status(500).json({ error: err.message })
        } else {
            res.json({ deleted: docs })
        }
    })
})

// lock an user
router.get('/users/lock/:id', async (req, res) => {
    const userId = req.params.id

    Usuario.findByIdAndUpdate(userId, { isLocked: true }, (err, docs) => {
        if (err) {
            console.log(err)
            res.status(422).json({ error: err.message })        
        } else {
            console.log(docs)
            res.json({ updated: docs })        
        }
    })
})

// unlock an user
router.get('/users/unlock/:id', async (req, res) => {
    const userId = req.params.id

    Usuario.findByIdAndUpdate(userId, { isLocked: false }, (err, docs) => {
        if (err) {
            console.log(err)
            res.status(422).json({ error: err.message })        
        } else {
            console.log(docs)
            res.json({ updated: docs })        
        }
    })
})

module.exports = router