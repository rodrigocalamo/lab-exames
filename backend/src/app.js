const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const ejs = require('ejs')

const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const app = express()

app.use(morgan('dev'))
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.set('view engine', 'ejs')

// Authentication Routes 
app.use(require('./routes/users'))
app.use(require('./routes/sign-up'))
app.use(require('./routes/email-confirmation'))
app.use(require('./routes/request-reset'))
app.use(require('./routes/sign-in'))

// Contact Us Routes
app.use(require('./routes/contact-us'))

//Exam Routes
app.use(require('./routes/exams'))

//Apontamento Routes
app.use(require('./routes/apontamento'))

//Scheduling Routes
app.use(require('./routes/scheduling'))

module.exports = app