const app = require('./app')
const { connect } = require('./database')
const PORT = process.env.PORT || 3001

async function main() {
    //Conexão com o banco
    await connect()

    await app.listen(PORT)
    console.log(`Server is running on port ${PORT}: Connected`)
}

main()

