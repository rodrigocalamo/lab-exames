const { Double } = require('mongodb')
const { Schema, model } = require('mongoose')

const examSchema = new Schema({
    name: String,
    amount: {
        currency: String,
        value: Number,
    },
    description: String
})

module.exports = model('Exames', examSchema)