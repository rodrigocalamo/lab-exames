const { Schema, model } = require('mongoose')

const userSchema = new Schema({
    email: String,
    firstName: String,
    lastName: String,
    administrator: {
        type: Boolean,
        default: false
    },
    isLocked: {
        type: Boolean,
        default: true
    },
    password: String
})

module.exports = model('Usuario', userSchema)