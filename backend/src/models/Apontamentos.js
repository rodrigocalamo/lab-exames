const { Schema, model } = require('mongoose')

const apontamentosSchema = new Schema({
    exam: {
        type: Schema.Types.ObjectId, ref: 'Exames'
    },
    schedule: {
        day: String,
        time: String,
    }
})

module.exports = model('Apontamentos', apontamentosSchema)