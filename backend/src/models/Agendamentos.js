const { Schema, model } = require('mongoose')

const agendamentosSchema = new Schema({
    exam: {
        type: Schema.Types.ObjectId, ref: 'Exames'
    },
    user: {
        type: Schema.Types.ObjectId, ref: 'Usuario'
    },
    schedule: {
        day: String,
        time: String,
    }
})

module.exports = model('Agendamentos', agendamentosSchema)