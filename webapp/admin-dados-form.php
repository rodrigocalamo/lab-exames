<?php
require('functions.php');
?>

<h2><strong>Editar Registro</strong></h2>
  <p>Digite novas informações para atualizar seu cadastro</p><br><hr><br>
        <form action="<?= action('handle-admin-update-data.php') ?>" method="post">
        <fieldset class="grupo">
            <div class="campo">
                <label for="text"><strong>Digite o e-mail de quem você quer alterar os dados:</strong></label>
                <input type="email" name="email" placeholder="email@email.com" value="<?= $_POST['email'] ?>" required="true">
            </div>
            <div class="campo">
                <label for="firstName"><strong>Nome:</strong></label>
                <input type="text" class="firstName" name="firstName" placeholder="Nome">
            </div>
            <div class="campo">
                <label for="lastName"><strong>Sobrenome:</strong></label>
                <input type="text" class="lastName" name="lastName" placeholder="Sobrenome">
            </div>
            <div class="campo">
                <label for="administrator">Administrador?</label>
                <select name="administrator" id="administrator">
                            <option value="false">Não</option>
                            <option value="true">Sim</option>
                </select>
            </div>
            <div class="campo">
                <label for="isLocked">Bloqueado?</label>
                <select name="isLocked" id="isLocked">
                            <option value="false">Não</option>
                            <option value="true">Sim</option>
                </select>
            </div>
        </fieldset>
        <fieldset class="grupo">
            <div class="campo">
                <label for="password"><strong>Digite uma nova senha:</strong></label>
                <input type="password" name="password" placeholder="Senha" >
            </div>
        </fieldset><br>        
            <input type="submit" value="Alterar Dados" class="botao">
        </form>