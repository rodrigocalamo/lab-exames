<?php
require('functions.php');

$schedules = fetch_data('http://localhost:3001', '/agendamentos');
?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav-admin.php') ?>
        <main>
        <h2><strong>Consultas Registradas</strong></h2><br><hr><br>
        <session class="tab-exames">
                <table class="table-exames">
                    <th>Exame</th>
                    <th>Data</th>
                    <th>Hora</th>
                    <th>Paciente</th>
                    <?php foreach ($schedules as $sch) { ?>
                        <tr>
                            <td><?= $sch->examName ?></td>
                            <td><?= $sch->schedule->schedule->day ?></td>
                            <td><?= $sch->schedule->schedule->time ?></td>
                            <td><?= $sch->firstName . " " . $sch->lastName ?></td>
                        </tr>
                   <?php } ?>
                </table>
            </session>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
