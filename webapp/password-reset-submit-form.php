<?php
require('functions.php');
?>

<div class="">
    <h2><strong>Redefinição de Senha</strong></h2><br>
    <form action="<?= action('handle-password-reset-submit.php') ?>" method="post">
        <input type="text" name="id" value="<?= $_GET['id'] ?>" hidden>
        <input type="text" name="email" value="<?= $_GET['email'] ?> " hidden>
        <div class="grupo-centralizado">
            <fieldset class="grupo">
                <div class="campo">
                    <label for="password"><strong>Nova Senha:</strong></label>
                        <input type="password" class="" name="password" value="" placeholder="Nova senha" required="true" autocomplete="new-password" autofocus>
                </div>
            </fieldset>
        </div>
        <fieldset class="grupo">
            <div clas="campo">
                <input type="submit" class="botao">
            </div>
            <div class="campo">
                <p><a href="login.php">Voltar para Login</a></p>
            </div>
        </fieldset>
    </form>
</div>