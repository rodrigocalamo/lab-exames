<?php
require_once('setup.php');

$id = $_POST['id'];

$params = [
  'name' => $_POST['nameExam'],
  'amount' => [
    'currency' => 'R$',
    'value' => $_POST['valueExam']
  ],
  'description' => $_POST['description']
];

pp($_POST);

$result = post_data('http://localhost:3001', "/update/exams/$id", $params);
?>

<?php if ($result->resp == 'updated') { ?>
    <script>
     alert('Exame atualizado com sucesso.');
     window.location.href = 'admin-exames.php';
    </script>
<?php
}