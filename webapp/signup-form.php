<?php
require('functions.php');
?>

<div class="">
    <h2><strong>Registrar-se</strong></h2>
    <p>Digite suas informações para concluir o cadastro</p><br>
    <form action="<?= action('handle-signup.php') ?>" method="post">
        <fieldset class="grupo">
            <div class="campo">
                <label for="firstName"><strong>Nome:</strong></label>
                <input type="text" class="firstName" name="firstName" placeholder="Nome" value="<?=$_POST['firstName'] ?>" required="true">
            </div>
            <div class="campo">
                <label for="lastName"><strong>Sobrenome:</strong></label>
                <input type="text" class="lastName" name="lastName" placeholder="Sobrenome" value="<?=$_POST['lastName'] ?>" required="true">
            </div>
        </fieldset>
        <fieldset class="grupo">
            <div class="campo">
                <label for="email"><strong>Email:</strong></label>
                <input type="text" class="email" id="sign-up-email" name="email" placeholder="E-mail" value="<?=$_POST['email'] ?>" required="true">
            </div>
            <div class="campo">
                <label for="confirm_email"><strong>Confirmar Email:</strong></label>
                <input type="email" name="confirm_email" id="confirm_email" placeholder="Confirmar E-mail" required>
            </div>
        </fieldset>
        <fieldset class="grupo">
            <div class="campo">
                <label for="password"><strong>Senha:</strong></label>
                <input type="password" class="" id="sign-up-password" name="password" placeholder="Senha" value="<?=$_POST['password'] ?>" required="true" autocomplete="new-password">
            </div>
            <div class="campo">
                <label for="confirm_password"><strong>Confirmar Senha:</strong></label>
                <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirmar Senha" required>
            </div>
        </fieldset> 
        <input type="submit" class="botao" onclick="validar_signup_form()">
    </form>
</div>

