<?php
require('functions.php');

$id = $_GET['id'];

$exams = fetch_data('http://localhost:3001', "/exams/$id");
?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav-admin.php') ?>
        <main>
        <h2><strong>Editando Exame</strong></h2>
            <p>Digite as informações para editar o exame</p><br><hr><br>
            <form action="<?= action('handle-admin-update-exam.php') ?>" method="post">
                <fieldset class="grupo">
                    <div class="campo">
                        <label for="nameExam"><strong>Nome do Exame:</strong></label>
                        <input type="text" name="nameExam" id="nameExam" value="<?= $exams->name ?>">
                    </div>
                    <div class="campo">
                        <label for="valueExam"><strong>Valor do Exame</strong></label>
                        <input type="text" name="valueExam" id="valueExam" value="<?= $exams->amount->value ?>">
                    </div>
                </fieldset>
                <div>
                    <fieldset class="grupo">
                        <div class="campo">
                            <label for="description" class="label-descricao"><strong>Descrição:</strong></label>
                            <input class="descricao" name="description" placeholder="Descrição do exame" value="<?= $exams->description ?>">
                        </div>
                    </fieldset>
                </div>
                <input type="text" name="id" value="<?= $_GET['id'] ?>" hidden>
                <input type="submit" class="botao">
            </form>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
