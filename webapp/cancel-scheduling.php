<?php
require('functions.php');

if (isset($_GET['scheduleId'])) {
    $scheduleId = $_GET['scheduleId'];
    
    $cancelResult = fetch_data('http://localhost:3001', "/cancelar-agendamento/$scheduleId");
}

?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav.php') ?>
        <main class="main-diminuida">
        <div class="balao">
            <fieldset>
                    <div class="campo">
                        <h3 class="msg-sucesso">Seu agendamento foi cancelado</h3>
                        <p>Para marcar outro agendamento, entre na sua conta</p>
                    </div>
            </fieldset>
            <fieldset>
                <div class="campo">
                    <p class="white"><a href="login.php">Voltar para o Login</a></p>
                </div>
            </fieldset>
        </div>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
