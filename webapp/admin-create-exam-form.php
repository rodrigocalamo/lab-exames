<?php
require('functions.php');
?>

<h2><strong>Adicionar Exame</strong></h2>
  <p>Digite as informações para adicionar o exame</p><br><hr><br>
    <form action="<?= action('handle-create-exam.php') ?>" method="post">
      <fieldset class="grupo">
        <div class="campo">
          <label for="addExame"><strong>Nome do Exame:</strong></label>
          <input type="text" name="examName" id="examName" value="<?= $_POST['examName'] ?>" required="true">
        </div>
        <div class="campo">
          <label for="valorExame"><strong>Valor do exame</strong></label>
          <input type="number" step="0.01" min="0" name="examValue" id="examValue" required="true">
        </div>
      </fieldset>
        <div>
          <fieldset class="grupo">
            <div class="campo">
              <label for="descricao" class="label-descricao"><strong>Descrição:</strong></label>
              <textarea class="descricao" name="description" id="description" required="true" placeholder="Descrição do exame"></textarea>
            </div>
          </fieldset>
        </div>
        <input type="text" name="currency" id="currency" value="R$" hidden="true">
        <input type="submit" class="botao">
    </form>