<?php
require_once('setup.php');

$email = $_POST['email'];
$params = [
	'firstName' => $_POST['firstName'],
	'lastName' => $_POST['lastName'],
	'email' => $email,
	'password' => $_POST['password']
];

$result = post_data('http://localhost:3001', '/sign-up', $params);

if ($result->resp === 'created') {
    $_SESSION['email'] = $email;
    redirect('signup.php');
} else { ?>
    <script>
    alert('Usuário já existente.');
    window.location.href = 'signup.php';
    </script>
<?php } ?>