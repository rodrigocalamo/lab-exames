<?php
require('functions.php');

$exams = fetch_data('http://localhost:3001', '/exams');

$newEmail = substr($_GET['email'], 0, -1);

$_SESSION['email'] = $newEmail;
?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('user-logged-nav.php') ?>
        <main class="main-container">
        <br/>
            <section>  
            <h1>Agendar exame </h1>
            <br/><hr/><br/>
                <label>Escolha o exame que deseja agendar:</label>
                <table class="table-exames">
                    <th>Exame</th>
                    <th>Valor</th>
                    <th>Orientações</th>
                    <th>Opções</th>
                    <?php foreach ($exams as $exam) { ?>
                        <tr>
                            <td><?= $exam->name ?></td>
                            <td><?= $exam->amount->currency . $exam->amount->value ?></td>
                            <td><?= $exam->description ?></td>
                            <td><a href="user-schedule-exam.php?id=<?= $exam->_id ?>&email=<?= $newEmail ?>"><input type="button" value="Agendar" class="botao"></a>
                        </tr>
                    <?php } ?>
                </table>
            </section>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>