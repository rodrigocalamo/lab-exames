<?php
require('functions.php');
session_start();

if (isset($_SESSION['email'])) {
    $email = $_SESSION['email'];
} 

?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('user-logged-nav.php') ?>
        <main class="main-container">

        <br/>
            <section>  
                <h1>Meus dados pessoais </h1>
                <br/><hr/><br/>
                <form action="<?= action('handle-update-user-information.php') ?>" method="post">
                    <p for="email">Email: <input type="text" name="email" id="email" value="<?= $email ?>" disabled><br/><br/></p>
                    <p for="senhaAtual">Senha Atual: <input type="password" id="currentPassword" name="currentPassword" ><br/><br/></p>
                    <p for="novaSenha">Nova Senha: <input type="password" id="newPassword" name="newPassword"><br/><br/></p>
                    <button type="submit" class="botao">Alterar</button>
                </form>
            </section>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>