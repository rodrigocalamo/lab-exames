<?php

function pp($object) {
    error_log(print_r($object, TRUE));
    if ($_SERVER['REQUEST_METHOD'] === 'GET' && $_SERVER['REMOTE_ADDR'] === '::1') {
        echo '<pre>';
        print_r($object);
        echo '</pre>';
    }
}

function fetch_data($url, $route, $params = NULL) {
    if (empty($url))
        error_log("fetch_data: url is empty");
    
    $full_url = $url . $route;
    if ($params != NULL) {
        $full_url .= '?' .http_build_query($params);
    }
    $ch = curl_init($full_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($response);
    return $result;
}

function post_data($url, $route, $data, $params = NULL) {
    $ch = curl_init($url . $route);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    $response = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($response);
    return $result;
}

function action($filename, $keepId = true) {
    return $keepId ? "$filename?={$_SESSION['id']}" : $filename;
}

function redirect($filename, $params = []) {
    $urlParams = array_merge($params, $_GET);
    header("Location: $filename?" .http_build_query($urlParams));
}

function get_current_script() {
    $data = explode("/", $_SERVER['SCRIPT_NAME']);
    return end($data);
}