function recursiva_menu() {
    var x = document.getElementById("nav");

    if (x.className === "nav") {
      x.className += " responsive";
    } else {
      x.className = "nav";
    }
  }

  function validar_signup_form() {
    const password = document.getElementById("sign-up-password");
    const confirm_password = document.getElementById("confirm_password");
    const email = document.getElementById("sign-up-email");
    const confirm_email = document.getElementById("confirm_email");

    var emailValidation = false
    var passwordValidation = false

    if (email.value !== confirm_email.value) {
      confirm_email.setCustomValidity("Os e-mails não correspondem.")
    } else {
      emailValidation = true
      confirm_email.setCustomValidity('')
    }

    if (password.value !== confirm_password.value) {
      confirm_password.setCustomValidity("As senhas não correspondem.")
    } else {
      passwordValidation = true
      confirm_password.setCustomValidity('')
    }

    if (emailValidation && passwordValidation) return
  }