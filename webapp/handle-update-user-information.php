<?php
require_once('setup.php');
session_start();

if (isset($_SESSION['email'])) {
  $email = $_SESSION['email'];

  $emailParams = [
    'email' => $email
  ];
  
  $contact = post_data('http://localhost:3001', '/users/find/email', $emailParams);
  
  $params = [
    'email' => $email,
    'firstName' => $contact->firstName,
    'lastName' => $contact->lastName,
    'administrator' => false,
    'isLocked' => false,
    'password' => $_POST['newPassword'],  
  ];

  if ($contact->password == $_POST['currentPassword']) {
    $result = post_data('http://localhost:3001', '/users/email', $params);
    ?>
  
    <?php if ($result->contact == 'updated') { ?>
      <script>
       alert('Contato atualizado com sucesso.');
       window.location.href = 'user-logged.php';
      </script>
    <?php
    }
  } else { ?>
    <script>
      alert('Sua senha atual não confere com a que acabou de digitar. Tente novamente.');
      window.location.href = 'user-logged.php';
    </script>
  <?php } 
}