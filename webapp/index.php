<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav.php') ?>
        <main class="main-container">
            <session>
                <h1> Sobre a ExamLab </h1>
                <br/>
                <article class="article1">
                    <p> 
                        A Exam Lab é um laboratório especializado em divesos tipos de exames.
                        Possuimos um centro laboratorial com os equipamentos mais modernos do setor.
                        Na Exam Lab você pode realizar seu cadastro e agendar exames com rapidez e facilidade.
                        Além disso você pode consultar todos os exames e valores na nossa tabela de preços.
                    </p>    
                    <br/>
                    <img src="./imagens/laboratorio.jpg" />
                </article>         
            </session>

        </main>
        <?php include('footer.php') ?>
    </body>
</html>
