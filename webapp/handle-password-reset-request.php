<?php
require_once('setup.php');

$email = $_POST['email'];
$result = post_data('http://localhost:3001', '/request-reset', ['email' => $email]);
?>

<script>
 alert('Se este usuário estiver registrado, um e-mail será enviado com um link para a mudança da senha.');
 window.location.href = 'login.php';
</script>
