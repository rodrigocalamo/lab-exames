<?php
require('functions.php');

$users = fetch_data('http://localhost:3001', '/users/all');

?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav-admin.php') ?>
        <main class="main-container">
            <h2> Usuários </h2> <br/>
            <p>Veja na tabela abaixo todos os usuários registrados em nosso banco de dados.</p><br/>
            <hr/> <br/>
            <session class="tab-exames">
                <table class="table-exames">
                    <th>Nome</th>
                    <th>Sobrenome</th>
                    <th>Administrador</th>
                    <th>Bloqueado</th>
                    <th>E-mail</th>
                    <?php foreach ($users as $user) { ?>
                        <tr>
                            <td><?= $user->firstName ?></td>
                            <td><?= $user->lastName ?></td>
                            <?php if ($user->administrator == 1) { ?>
                                <td>Sim</td>
                            <?php } else { ?>
                                <td>Não</td>
                                <?php } ?>
                            <?php if ($user->isLocked == 1) { ?>
                                <td>Sim</td>
                            <?php } else { ?>
                                <td>Não</td>
                                <?php } ?>
                            <td><?= $user->email ?></td>
                        </tr>
                    <?php } ?>
                </table>
                <br/>
            </session>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>