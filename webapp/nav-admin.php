<header>
        <img src="imagens/favicon_lab.png" alt="Logo">
        <h1>Exam Lab: Laboratorio de Exames</h1>
</header>
<nav class="nav" id="nav">
    <a href="admin.php">Area Logada</a>
    <a href="admin-exames.php">Exames</a>
    <a href="admin-all-users.php">Usuários</a>
    <a href="admin-consultas.php">Consultas</a>
    <a href="admin-dados.php">Dados Cadastrais</a>
    <a class="nav_log" href="login.php">Sair</a>
    <a href="javascript:void(0);" class="icon" onclick="recursiva_menu()">
        <img src="imagens/opcao_menu.png" alt="Barra Menu">
    </a>
</nav>
