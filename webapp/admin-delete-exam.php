<?php
require('functions.php');

$id = $_GET['id'];

$exams = fetch_data('http://localhost:3001', "/exams/$id");
?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav-admin.php') ?>
        <main>
        <h2><strong>Excluindo Exame</strong></h2>
            <p>Digite as informações para editar o exame</p><br><hr><br>
            <form action="<?= action('handle-admin-delete-exam.php') ?>" method="post">
              <fieldset class="grupo">
                  <div class="campo">
                      <label for="addExame"><strong>Nome do Exame:</strong></label>
                      <input type="text" name="addExame" id="addExame" value="<?= $exams->name ?>" disabled>
                  </div>
                  <div class="campo">
                      <label for="valorExame"><strong>Valor do exame</strong></label>
                      <input type="text" name="valorExame" id="valorExame" value="<?= $exams->amount->currency . $exams->amount->value ?>" disabled>
                  </div>
              </fieldset>
              <div>
                  <fieldset class="grupo">
                      <div class="campo">
                          <label for="descricao" class="label-descricao"><strong>Descrição:</strong></label>
                          <input type="text" class="descricao" name="descricao" placeholder="Descrição do exame" value="<?= $exams->description ?>" disabled>
                      </div>
                  </fieldset>
              </div>
              <input type="text" name="id" value="<?= $_GET['id'] ?>" hidden>
              <input type="submit" value="Excluir" class="botao-excluir">
            </form>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
