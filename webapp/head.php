<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Lab: Laboratorio de Exames</title>
    <link rel="shortcut icon" href="imagens/favicon_lab.png" type="image/x-icon">
    <link rel="stylesheet" href="estilo/estilo.css">
    <script src="script/script.js"></script>
</head>