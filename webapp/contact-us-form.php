<?php
require('functions.php');
?>

<div class="">
    <h2><strong>Fale Conosco</strong></h2><br>
        <form action="<?= action('handle-contact-us.php') ?>" method="post">
            <div class="grupo-centralizado">
                <fieldset class="grupo">
                    <div class="campo">
                        <label for="Nome"><strong>Email:</strong></label>
                        <input type="text" name="email" id="email" placeholder="Email" value="<?= $_POST['email'] ?>" required="true">
                    </div>
                    <div class="campo">
                        <label for="name"><strong>Nome Completo:</strong></label>
                        <input type="text" name="name" id="name" placeholder="Nome Completo" required="true">
                    </div>
                    <div class="campo">
                        <label for="assunto"><strong>Assunto:</strong></label>
                        <input type="text" name="subject" id="subject" placeholder="Assunto" required>
                    </div>
                </fieldset>
            </div>
            <div class="grupo-centralizado">
                <fieldset class="grupo">
                    <div class="campo">
                        <label for="descricao" class="label-descricao"><strong>Descrição:</strong></label>
                        <textarea class="descricao" name="description" id="description" placeholder="Descrição do assunto"></textarea>
                    </div>
                </fieldset>
            </div>
            <input type="submit" class="botao">
        </form>
</div>