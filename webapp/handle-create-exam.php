<?php
require_once('setup.php');

$examName = $_POST['examName'];
$params = [
  'name' => $examName,
	'amount' => [
    'currency' => $_POST['currency'],
    'value' => $_POST['examValue']
  ],
	'description' => $_POST['description']
];

$result = post_data('http://localhost:3001', '/exams', $params);
?>

<?php if ($result->resp == 'created') { ?>
  <script>
   alert('Exame criado com sucesso.');
   window.location.href = 'admin-create-exam.php';
  </script>
<?php
}