<?php
require('functions.php');

$exams = fetch_data('http://localhost:3001', '/exams');
$schedules = fetch_data('http://localhost:3001', '/agendamentos');

?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav-admin.php') ?>
        <main>
            <h1>Area logada Admin</h1><br><hr>
            <br><h2><strong>Consultas Marcadas</strong></h2><br>
            <session class="tab-exames">
                <table class="table-exames">
                    <th>Exame</th>
                    <th>Data</th>
                    <th>Hora</th>
                    <th>Paciente</th>
                    <?php foreach ($schedules as $sch) { ?>
                        <tr>
                            <td><?= $sch->examName ?></td>
                            <td><?= $sch->schedule->schedule->day ?></td>
                            <td><?= $sch->schedule->schedule->time ?></td>
                            <td><?= $sch->firstName . " " . $sch->lastName ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </session><br><br><hr><br>
            <h2><strong>Exames Registrados</strong></h2><br>
			<session class="tab-exames">
                <table class="table-exames">
                    <th>Exame</th>
                    <th>Valor</th>
                    <th>Orientações</th>
                    <?php foreach ($exams as $exam) { ?>
                        <tr>
                            <td><?= $exam->name ?></td>
                            <td><?= $exam->amount->currency . $exam->amount->value ?></td>
                            <td><?= $exam->description ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </session>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
