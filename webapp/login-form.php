<?php
require('functions.php');
?>

<div class="">
    <h2><strong>Login</strong></h2><br>
    <form action="<?= action('handle-login.php') ?>" method="post">
        <div class="grupo-centralizado">
            <fieldset class="grupo">
                <div class="campo">
                    <label for="email"><strong>Email</strong></label>
                        <input type="text" name="email" placeholder="Email" value="<?= $_POST['email'] ?>">
                </div>
                <div class="campo">
                    <label for="password"><strong>Senha</strong></label>
                        <input type="password" class="" name="password" placeholder="Senha" required="true" autofocus>
                </div>
            </fieldset>
            <fieldset class="grupo">
                <div class="campo">
                    <p id="password-reset-link"><a href="password-reset-request.php">Esqueceu sua senha?</a></p>
                </div>
                    <div class="campo">
                        <p><a href="signup.php">Registre-se aqui</a></p><br>
                    </div>
            </fieldset>
        </div>  
            <input type="submit" class="botao">
    </form>
<div>