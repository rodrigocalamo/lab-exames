<?php
require_once('setup.php');

$email = $_POST['email'];
$params = [
  'email' => $email, 
  'firstName' => $_POST['firstName'], 
  'lastName' => $_POST['lastName'], 
  'administrator' => $_POST['administrator'],
  'isLocked' => $_POST['isLocked'],
  'password' => $_POST['password'],  
];
$result = post_data('http://localhost:3001', '/users/email', $params);
?>

<?php if ($result->contact == 'updated') { ?>
    <script>
     alert('Contato atualizado com sucesso.');
     window.location.href = 'admin-dados.php';
    </script>
<?php
}