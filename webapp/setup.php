<?php

require_once('functions.php');
session_start();

$script = get_current_script();
$permitted_actions = [
    'admin-all-users.php',
    'admin-consultas.php',
    'admin-addconsulta.php',
    'admin-create-exam.php',
    'admin-delete-exam.php',
    'admin-editconsulta.php',
    'admin-exames.php',
    'contact-us.php',
    'exames.php',
    'handle-create-exam.php',
    'handle-admin-update-exam.php',
    'handle-admin-delete-exam.php',
    'handle-admin-update-data.php',
    'handle-contact-us.php',
    'handle-login.php',
    'handle-schedule-exam.php',
    'handle-signup.php',
    'handle-password-reset-request.php',
    'handle-password-reset-submit.php',
    'handle-update-user-information.php',
    'index.php',
    'login.php',
    'password-reset-request.php',
    'password-reset-submit.php',
    'user-logged-exams.php',
    'user-logged-nav.php',
    'user-logged-personal.php',
    'user-logged.php',
    'user-schedule-exam.php',
    'signup.php'
];

if (!in_array($script, $permitted_actions)) {
     header("Location: login.php");
     exit();
}
?>