<?php
require('functions.php');
session_start();

if (isset($_SESSION['email'])) {
    $email = $_SESSION['email'];

    $emailParams = [
        'email' => $email
    ];

    $contact = post_data('http://localhost:3001', '/users/find/email', $emailParams);

    $schedules = fetch_data('http://localhost:3001', "/agendamentos/$contact->_id");
}
?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('user-logged-nav.php') ?>
        <main class="main-container">

        <br/>
            <section>  
                <h1>Meus exames agendados </h1>
                <br/><hr/><br/>
                <table class="table-exames">
                    <th>Exame</th>
                    <th>Data</th>
                    <th>Hora</th>
                    <th>Nome Completo</th>
                    <?php foreach ($schedules as $sch) { ?>
                        <tr>
                            <td><?= $sch->examName ?></td>
                            <td><?= $sch->schedule->schedule->day ?></td>
                            <td><?= $sch->schedule->schedule->time ?></td>
                            <td><?= $sch->firstName . " " . $sch->lastName ?></td>
                        </tr>
                   <?php } ?>
                </table>
            </section>

        </main>
        <?php include('footer.php') ?>
    </body>
</html>