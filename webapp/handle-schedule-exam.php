<?php
require_once('setup.php');

$email = $_POST['email'];

$emailParams = [
  'email' => $email
];

$retrieve_userId = post_data('http://localhost:3001', '/users/find/email', $emailParams);

$params = [
  'examId' => $_POST['id'],
	'userId' => $retrieve_userId->_id,
  'schedule' => [
    'day' => $_POST['day'],
    'time' => $_POST['time']
  ]
];

$result = post_data('http://localhost:3001', '/create/agendamento', $params);
?>

<?php if ($result->resp == 'Agendamento criado com sucesso') { ?>
  <script>
   alert('Agendamento criado com sucesso, você deve receber um e-mail de confirmação em segundos.');
   window.location.href = 'user-logged.php';
  </script>
<?php
}