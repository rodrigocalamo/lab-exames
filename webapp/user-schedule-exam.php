<?php
require('functions.php');

$id = $_GET['id'];

$email = $_GET['email'];

$exams = fetch_data('http://localhost:3001', "/exams/$id");
$apontamentos = fetch_data('http://localhost:3001', "/apontamentos/$id");
?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('user-logged-nav.php') ?>
        <main>
        <h2><strong>Marcando uma consulta</strong></h2>
            <p>Digite as informações para editar o exame</p><br><hr><br>
            <form action="<?= action('handle-schedule-exam.php') ?>" method="post">
                <fieldset class="grupo">
                    <div class="campo">
                        <label for="nameExam"><strong>Nome do Exame:</strong></label>
                        <input type="text" name="nameExam" id="nameExam" value="<?= $exams->name ?>" disabled>
                    </div>
                    <div class="campo">
                        <label for="valueExam"><strong>Valor do Exame</strong></label>
                        <input type="text" name="valueExam" id="valueExam" value="<?= $exams->amount->value ?>" disabled>
                    </div>
                      <?php foreach ($apontamentos as $apt) { ?>
                        <div class="campo">
                              <label for="<?= $apt->_id ?>"><?= "Dia: " . $apt->schedule->day . " às " .  $apt->schedule->time ?></label>
                              <input type="radio" id="<?= $apt->_id ?>" name="schedules" value="<?= $apt->_id ?>">
                              <input class="hiddenFields" type="text" id="day" name="day" value="<?= $apt->schedule->day ?>">
                              <input class="hiddenFields" type="text" id="time" name="time" value="<?= $apt->schedule->time ?>">
                        </div>
                      <?php } ?>
                </fieldset>
                <div>
                    <fieldset class="grupo">
                        <div class="campo">
                            <label for="description" class="label-descricao"><strong>Descrição:</strong></label>
                            <input class="descricao" name="description" placeholder="Descrição do exame" value="<?= $exams->description ?>" disabled>
                        </div>
                        <div class="campo">
                            <label for="email"><strong>Confirme seu e-mail:</strong></label>
                            <input id="email" name="email" placeholder="a@a.com">
                        </div>
                    </fieldset>
                </div>
                <input type="text" name="id" value="<?= $_GET['id'] ?>" hidden>
                <input type="submit" class="botao">
            </form>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
