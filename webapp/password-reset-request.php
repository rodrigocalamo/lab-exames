<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav.php') ?>
        <main class="main-diminuida">
            <?php include('password-reset-request-form.php')?>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
