<?php
require('functions.php');
?>
<div class="">
    <h2><strong>Redefinição de Senha</strong></h2><br>
    <form action="<?= action('handle-password-reset-request.php') ?>" method="post">
        <div class="grupo-centralizado">
            <fieldset class="grupo">
                <div class="campo">
                    <label for="email"><strong>Digite o e-mail</strong></label>
                        <input type="email" class="" name="email" placeholder="E-mail" value="<?=$_POST['email'] ?>" required="true" autofocus>
                </div>
            </fieldset>
        </div>
        <fieldset class="grupo">
            <div clas="campo">
                <input type="submit" class="botao">
            </div>
            <div class="campo">
                <p><a href="login.php">Voltar para Login</a></p>
            </div>
        </fieldset>
    </form>
</div>