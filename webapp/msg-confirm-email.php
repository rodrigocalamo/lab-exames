<?php
require('functions.php');

if (isset($_GET['userId'])) {
    $userId = $_GET['userId'];
    
    $unlockResult = fetch_data('http://localhost:3001', "/email-confirmation/$userId");
}

?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav.php') ?>
        <main class="main-diminuida">
        <div class="balao">
            <fieldset>
                    <div class="campo">
                        <h3 class="msg-sucesso">Parabéns seu cadastro foi concluído!!</h3>
                        <p>Por favor, faça o seu login para acessar sua área logada</p>
                    </div><br>
            </fieldset>
            <fieldset>
                <div class="campo">
                    <p class="white"><a href="login.php">Voltar para o Login</a></p>
                </div>
            </fieldset>
        </div>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
