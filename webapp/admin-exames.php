<?php
require('functions.php');

$exams = fetch_data('http://localhost:3001', '/exams');
?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav-admin.php') ?>
        <main>
        <h2><strong>Exames Registrados</strong></h2><br><hr><br>
        <a href="admin-create-exam.php"><input type="button" value="Adicionar Exame" class="botao"></a><br><br><hr><br>
        <session class="tab-exames">
                <table class="table-exames">
                    <th>Exame</th>
                    <th>Valor</th>
                    <th>Orientações</th>
                    <th>Opções</th>
                    <?php foreach ($exams as $exam) { ?>
                        <tr>
                            <td><?= $exam->name ?></td>
                            <td><?= $exam->amount->currency . $exam->amount->value ?></td>
                            <td><?= $exam->description ?></td>
                            <td><a href="admin-editexam.php?id=<?= $exam->_id ?>"><input type="button" value="Editar" class="botao"></a>
                            <a href="admin-delete-exam.php?id=<?=$exam->_id ?>"><input type="button" value="Excluir" class="botao-excluir"></a>
                        </tr>
                    <?php } ?>
                </table>
            </session>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
