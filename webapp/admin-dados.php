<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav-admin.php') ?>
        <main>
            <?php include('admin-dados-form.php')?>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
