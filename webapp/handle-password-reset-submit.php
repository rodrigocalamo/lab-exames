<?php
require_once('setup.php');

$params = [
    'id' => $_POST['id'],
    'password' => $_POST['password']
];

$result = post_data('http://localhost:3001', '/reset-password', $params);
?>

<script>
 alert('Sua senha foi alterada com sucesso.');
 window.location.href = 'login.php';
</script>