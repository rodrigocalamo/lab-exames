<?php
require('functions.php');

$exams = fetch_data('http://localhost:3001', '/exams');
?>

<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav.php') ?>
        <main class="main-container">
            <h1> Exames </h1> <br/>
            <p>Veja na tabela abaixo todos os exames oferecidos pela Exam Lab.</p><br/>
            <hr/> <br/>
            <session class="tab-exames">
                
                <table class="table-exames">
                    <th>Exame</th>
                    <th>Valor</th>
                    <th>Orientações</th>
                    <?php foreach ($exams as $exam) { ?>
                        <tr>
                            <td><?= $exam->name ?></td>
                            <td><?= $exam->amount->currency . $exam->amount->value ?></td>
                            <td><?= $exam->description ?></td>
                        </tr>
                    <?php } ?>
                </table>
                <br/>
                <button id="redirectToLogin">Agendar um exame</button>
            </session>
            
            
        </main>
        <?php include('footer.php') ?>
    </body>
</html>

<script>
    const button = document.getElementById('redirectToLogin')
    button.addEventListener('click', () => {
        document.location.href = 'login.php'
    })
</script>