<?php
require_once('setup.php');
session_start();

$email = $_POST['email'];
$params = ['email' => $email, 'password' => $_POST['password']];
$result = post_data('http://localhost:3001', '/sign-in', $params);
?>

<?php if ($result->userData == '') { ?>
    <script>
     alert('E-mail/Senha incorretos.');
     window.location.href = 'login.php';
    </script>
<?php
} else {
    $_SESSION['email'] = $email;
    if ($result->userData == 'inactive') { ?>
        <script>
        alert('Usuário bloqueado, confirme seu e-mail clicando no link.');
        window.location.href = 'login.php';
        </script>
        <?php
        $verify_result = post_data('http://localhost:3001', '/re-send-email', ['email' => $email]);
    } else if ($result->userData == 'administrator') {
        redirect('admin.php');
    } else {
        redirect('user-logged.php?email=' . $email);
    }
}