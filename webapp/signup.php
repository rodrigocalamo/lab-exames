<!DOCTYPE html>
<html lang="pt-br">
    <?php include('head.php') ?>
    <body>
        <?php include('nav.php') ?>
        <main>
            <?php include('signup-form.php')?>
        </main>
        <?php include('footer.php') ?>
    </body>
</html>
